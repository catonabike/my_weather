<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ForecastPoint extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'date' => $this->dt,
            'main' => ForecastPointMain($this->main),
            'weather' => ForecastPointWeather($this->weather),
            'clouds' => ForecastPointClouds($this->clouds),
            'wind' => ForecastPointWind($this->wind),
            'sys' => ForecastPointSys($this->sys),
            'date_text' => $this->dt_txt,
        ];
    }
}
