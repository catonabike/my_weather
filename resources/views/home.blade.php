@extends('layouts.app')

@section('content')
<div class="m-4">
    <form action="/weather_for" method="post" autocomplete="off">
    @csrf
        <div class="form-group autocomplete">
            <label for="towm">Enter Town or City to get forecast for:</label>  
            <input type="text" class="form-control" id="town" name="town">
            <input type="hidden" id="town-id">
            <div id="town-list"></div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection

@section('result')
<div class="container">
    <h4 class="text-primary"> 5 day weather forecast for {{ $result['city']['name'] ?? '' }}</h4>
</div>
<div class="container">
    <table class="table table-fixed table-striped table-bordered text-nowrap">
        <thead>
            <tr>
                <th>Date & Time</th>
                <th>Temperature</th>
                <th>Wind Speed</th>
                <th>Wind Direction</th>
                <th>Description</th>
                <th>ICON:</th>
            </tr>
        </thead>
        <tbody>
        @if( $result ?? 0 )
            @foreach( $result['list'] as $fc )
                <tr>
                    <td>{{ $fc['dt_txt'] }}</td>
                    <td>{{ $fc['main']['temp'] }}</td>
                    <td>{{ $fc['wind']['speed'] }}</td>
                    <td>{{ $fc['wind']['deg'] }}</td>
                    <td>{{ $fc['weather'][0]['description'] }}</td>
                    <td><img src="storage/{{ $fc['weather'][0]['icon'] }}@2x.png"></td>
                </tr>
            @endforeach
        @else
            <tr>
                <td>NO</td>
                <td>DATA</td>
                <td>VAILABLE</td>
                <td>JUST</td>
                <td>NOW!</td>
                <td>n/a</td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
@endsection

@push('scripts')
    <script>
        // CSRF Token
        var CSRF_TOKEN = $( 'meta[name="csrf-token"]' ).attr( 'content' );

        $( document ).ready( function() {
            $( '#town' ).autocomplete({
                source: function( request, response ){
                    $.ajax({
                        url: "{{ route( 'autocomplete' ) }}",
                        type: "post", // NB: documentation says this should be method:"post" but that causes error!
                        dataType: "json",
                        data: {
                            search: request.term, // So 'search' is what is sent through, ie $request->search
                            _token: CSRF_TOKEN
                        },
                        success: function(data){
                            response( data );
                        }
                    });
                },
                select: function( event, ui ){
                    $('#town').val(ui.item.label);
                    $('#town-id').val(ui.item.value);
                    return false;
                }
            });
        });
    </script>
@endpush
