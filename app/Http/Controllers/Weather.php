<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ForecastResource;
use GuzzleHttp\Client;

class Weather extends Controller
{
    /**
     * Shows the home view with a form to input a place.
     * 
     * @return View
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the weather data for a given location.
     *
     * @param  \Illuminate\Http\Request $request
     * @return View
     */
    public function show(Request $request)
    {
        $base = env('OWM_BASE_URL');
        $key = env('OWM_KEY');
        $url = $base."q=".$request->town.",gb&units=metric&appid=".$key;

        $client = new Client();
        $response = $client->request('GET', $url);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();

        $result = json_decode($body, true);

        return view('home', [ 'result' => $result ]);
    }

    public function autocomplete(Request $request)
    {
        $base = env('API_BASE_URL');
        $client = new Client();
        $query = $request->search;
        $response = array();
        
        $response = $client->request('GET', $base."places/search/".$query);
        $statusCode = $response->getStatusCode();
        $body = $response->getBody()->getContents();
        $json = json_decode( $body, true);
        
        
        foreach( $json['city'] as $place )
        {
            $output[] = array("value" => $place['cityRef'], "label" => $place['cityName']);
        }
        
        echo json_encode($output);
    }
}
