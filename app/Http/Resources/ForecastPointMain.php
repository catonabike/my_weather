<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ForecastPointMain extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'temp' => $this->temp,
            'feels_like' => $this->feels_like,
            'temp_min' => $this->temp_min,
            'temp_max' => $this->temp_max,
            'pressure' => $this->pressure,
            'sea_level' => $this->sea_level,
            'ground_level' => $this-> grnd_level,
            'humidity' => $this->humidity,
            'temp_kf' => $this->temp_kf,
        ];
    }
}
