<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ForecastCity extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'coord' => ForecastCityCo($this->coord),
            'country' => $this->country,
            'timezone' => $this->timezone,
            'sunrise' => $this->sunrise,
            'sunset' => $this->sunset,
        ];
    }
}
