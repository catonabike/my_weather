<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ForecastResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code' => $this->cod,
            'message' => $this->message,
            'count' => $this->cnt,
            'list' => ForecastPoint::collection($this->list),
            'city' => ForecastCity($this->city),
        ];
    }
}
